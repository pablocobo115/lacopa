<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "torneos".
 *
 * @property int $cod_torneo
 * @property string|null $nombre
 * @property string|null $formato
 *
 * @property Juegan[] $juegans
 */
class Torneos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'torneos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_torneo'], 'required'],
            [['cod_torneo'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['formato'], 'string', 'max' => 7],
            [['cod_torneo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_torneo' => 'Cod Torneo',
            'nombre' => 'Nombre',
            'formato' => 'Formato',
        ];
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['cod_torneo' => 'cod_torneo']);
    }
}
