<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arbitros".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $fecha
 *
 * @property Arbitran[] $arbitrans
 * @property Partidos[] $codPartidos
 */
class Arbitros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'arbitros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['fecha'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 30],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Arbitrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArbitrans()
    {
        return $this->hasMany(Arbitran::className(), ['dni' => 'dni']);
    }

    /**
     * Gets query for [[CodPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo' => 'cod_partido'])->viaTable('arbitran', ['dni' => 'dni']);
    }
}
