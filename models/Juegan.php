<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $codigo
 * @property int|null $cod_equipo
 * @property int|null $cod_partido
 * @property int|null $cod_torneo
 *
 * @property Equipos $codEquipo
 * @property Partidos $codPartido
 * @property Torneos $codTorneo
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_equipo', 'cod_partido', 'cod_torneo'], 'integer'],
            [['cod_partido', 'cod_equipo', 'cod_torneo'], 'unique', 'targetAttribute' => ['cod_partido', 'cod_equipo', 'cod_torneo']],
            [['cod_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['cod_equipo' => 'codigo']],
            [['cod_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['cod_partido' => 'codigo']],
            [['cod_torneo'], 'exist', 'skipOnError' => true, 'targetClass' => Torneos::className(), 'targetAttribute' => ['cod_torneo' => 'cod_torneo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'cod_equipo' => 'Cod Equipo',
            'cod_partido' => 'Cod Partido',
            'cod_torneo' => 'Cod Torneo',
        ];
    }

    /**
     * Gets query for [[CodEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo' => 'cod_equipo']);
    }

    /**
     * Gets query for [[CodPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo' => 'cod_partido']);
    }

    /**
     * Gets query for [[CodTorneo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodTorneo()
    {
        return $this->hasOne(Torneos::className(), ['cod_torneo' => 'cod_torneo']);
    }
}
