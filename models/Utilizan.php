<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizan".
 *
 * @property int $codigo
 * @property int|null $codj
 * @property string|null $nombre
 *
 * @property Jugadores $codj0
 * @property Personajes $nombre0
 */
class Utilizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codj'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['codj', 'nombre'], 'unique', 'targetAttribute' => ['codj', 'nombre']],
            [['codj'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codj' => 'codigo']],
            [['nombre'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['nombre' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'codj' => 'Codj',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Codj0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodj0()
    {
        return $this->hasOne(Jugadores::className(), ['codigo' => 'codj']);
    }

    /**
     * Gets query for [[Nombre0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombre0()
    {
        return $this->hasOne(Personajes::className(), ['nombre' => 'nombre']);
    }
}
