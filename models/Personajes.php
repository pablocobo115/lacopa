<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property string $nombre
 * @property string|null $rol_principal
 * @property int|null $stats_base
 * @property int|null $escalados
 *
 * @property Jugadores[] $jugadores
 * @property Utilizan[] $utilizans
 * @property Jugadores[] $codjs
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['stats_base', 'escalados'], 'integer'],
            [['nombre', 'rol_principal'], 'string', 'max' => 30],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'rol_principal' => 'Rol Principal',
            'stats_base' => 'Stats Base',
            'escalados' => 'Escalados',
        ];
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['nombre_personaje' => 'nombre']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::className(), ['nombre' => 'nombre']);
    }

    /**
     * Gets query for [[Codjs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodjs()
    {
        return $this->hasMany(Jugadores::className(), ['codigo' => 'codj'])->viaTable('utilizan', ['nombre' => 'nombre']);
    }
}
