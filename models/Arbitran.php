<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arbitran".
 *
 * @property int $codigo
 * @property int|null $cod_partido
 * @property string|null $dni
 *
 * @property Arbitros $dni0
 * @property Partidos $codPartido
 */
class Arbitran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'arbitran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_partido'], 'integer'],
            [['dni'], 'string', 'max' => 9],
            [['dni', 'cod_partido'], 'unique', 'targetAttribute' => ['dni', 'cod_partido']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Arbitros::className(), 'targetAttribute' => ['dni' => 'dni']],
            [['cod_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['cod_partido' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'cod_partido' => 'Cod Partido',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Arbitros::className(), ['dni' => 'dni']);
    }

    /**
     * Gets query for [[CodPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo' => 'cod_partido']);
    }
}
