<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $localización
 * @property string|null $nom_director
 * @property string|null $ape1
 * @property string|null $ape2
 *
 * @property Juegan[] $juegans
 * @property Jugadores[] $jugadores
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'localización', 'nom_director', 'ape1', 'ape2'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'localización' => 'Localización',
            'nom_director' => 'Nom Director',
            'ape1' => 'Ape1',
            'ape2' => 'Ape2',
        ];
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['cod_equipo' => 'codigo']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['cod_equipo' => 'codigo']);
    }
}
