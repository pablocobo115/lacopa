<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $codigo
 * @property int|null $cod_jugador
 * @property int|null $cod_equipo
 * @property string|null $rol
 * @property string|null $nombre
 * @property string|null $nombre_personaje
 *
 * @property Equipos $codEquipo
 * @property Personajes $nombrePersonaje
 * @property Nacionalidad[] $nacionalidads
 * @property Utilizan[] $utilizans
 * @property Personajes[] $nombres
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $Equipo;
    
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador', 'cod_equipo'], 'integer'],
            [['rol', 'nombre', 'nombre_personaje'], 'string', 'max' => 30],
            [['cod_jugador', 'cod_equipo'], 'unique', 'targetAttribute' => ['cod_jugador', 'cod_equipo']],
            [['cod_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['cod_equipo' => 'codigo']],
            [['nombre_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['nombre_personaje' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'cod_jugador' => 'Cod Jugador',
            'cod_equipo' => 'Cod Equipo',
            'rol' => 'Rol',
            'nombre' => 'Nombre',
            'nombre_personaje' => 'Nombre Personaje',
        ];
    }

    /**
     * Gets query for [[CodEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo' => 'cod_equipo']);
    }

    /**
     * Gets query for [[NombrePersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePersonaje()
    {
        return $this->hasOne(Personajes::className(), ['nombre' => 'nombre_personaje']);
    }

    /**
     * Gets query for [[Nacionalidads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidads()
    {
        return $this->hasMany(Nacionalidad::className(), ['codj' => 'codigo']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::className(), ['codj' => 'codigo']);
    }

    /**
     * Gets query for [[Nombres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombres()
    {
        return $this->hasMany(Personajes::className(), ['nombre' => 'nombre'])->viaTable('utilizan', ['codj' => 'codigo']);
    }
}
