<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidad".
 *
 * @property int $codigo
 * @property int|null $codj
 * @property string|null $nacionalidad
 *
 * @property Jugadores $codj0
 */
class Nacionalidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codj'], 'integer'],
            [['nacionalidad'], 'string', 'max' => 30],
            [['codj', 'nacionalidad'], 'unique', 'targetAttribute' => ['codj', 'nacionalidad']],
            [['codj'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codj' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'codj' => 'Codj',
            'nacionalidad' => 'Nacionalidad',
        ];
    }

    /**
     * Gets query for [[Codj0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodj0()
    {
        return $this->hasOne(Jugadores::className(), ['codigo' => 'codj']);
    }
}
