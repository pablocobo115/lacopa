<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $codigo
 * @property string|null $fecha
 * @property string|null $lado
 * @property string|null $resultado
 *
 * @property Arbitran[] $arbitrans
 * @property Arbitros[] $dnis
 * @property Juegan[] $juegans
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['lado'], 'string', 'max' => 10],
            [['resultado'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'fecha' => 'Fecha',
            'lado' => 'Lado',
            'resultado' => 'Resultado',
        ];
    }

    /**
     * Gets query for [[Arbitrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArbitrans()
    {
        return $this->hasMany(Arbitran::className(), ['cod_partido' => 'codigo']);
    }

    /**
     * Gets query for [[Dnis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnis()
    {
        return $this->hasMany(Arbitros::className(), ['dni' => 'dni'])->viaTable('arbitran', ['cod_partido' => 'codigo']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['cod_partido' => 'codigo']);
    }
}
