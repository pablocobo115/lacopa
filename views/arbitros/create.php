<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arbitros */

$this->title = 'Create Arbitros';
$this->params['breadcrumbs'][] = ['label' => 'Arbitros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arbitros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
