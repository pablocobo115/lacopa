<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_equipo')->textInput() ?>

    <?= $form->field($model, 'cod_partido')->textInput() ?>

    <?= $form->field($model, 'cod_torneo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
