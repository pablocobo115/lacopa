<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidad */

$this->title = 'Update Nacionalidad: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Nacionalidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nacionalidad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
