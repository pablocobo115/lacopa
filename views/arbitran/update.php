<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arbitran */

$this->title = 'Update Arbitran: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Arbitrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="arbitran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
