<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arbitran */

$this->title = 'Create Arbitran';
$this->params['breadcrumbs'][] = ['label' => 'Arbitrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arbitran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
