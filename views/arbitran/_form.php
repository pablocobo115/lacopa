<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Arbitran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="arbitran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_partido')->textInput() ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
