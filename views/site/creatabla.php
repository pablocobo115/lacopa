<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Crear Tabla';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-3 text-center well well-red">
        <h4>Tabla de 4 equipos<br></br></h4>
        <p class="text-center">
            <?= Html::a('Acceder', ['site/tabla4'], ['class'=>'btn btn-primary btn-block'])?>
        </p>
    </div>
    <div class="col-md-3 text-center well well-red">
        <h4>Tabla de 8 equipos<br></br></h4>

        <p>
            <?= Html::a('Acceder', ['site/tabla8'], ['class'=>'btn btn-primary btn-block'])?>
        </p>
    </div>
    <div class="col-md-3 text-center well well-red">
        <h4>Tabla de 16 equipos<br></br></h4>

        <p>
            <?= Html::a('Acceder', ['site/tabla16'], ['class'=>'btn btn-primary btn-block'])?>
        </p>
    </div>
</div>