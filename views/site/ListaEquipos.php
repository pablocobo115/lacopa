<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>
    <?= GridView::widget([
       'dataProvider' => $resultados,
        'columns' => $campos
    ]); ?> 
     <?= Html::a('Inscribir Equipo', ['equipos/create'], ['class'=>'btn btn-primary'])?>