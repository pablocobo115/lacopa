<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Torneos LaCopa</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-md-3 text-center well well-red">
                <h4>Listado de jugadores<br></br></h4>
                <p class="text-center">
                    <?= Html::a('Acceder', ['site/listajugadores'], ['class'=>'btn btn-primary btn-block'])?>
                </p>
            </div>
            <div class="col-md-3 text-center well well-red">
                <h4>Listado de equipos<br></br></h4>

                <p>
                    <?= Html::a('Acceder', ['site/listaequipos'], ['class'=>'btn btn-primary btn-block'])?>
                </p>
            </div>
            <div class="col-md-3 text-center well well-red">
                <h4>Creación de tablas de torneo<br></br></h4>

                <p>
                    <?= Html::a('Acceder', ['site/creatabla'], ['class'=>'btn btn-primary btn-block'])?>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 text-center well well-red">
                <h4>Texto de Ejemplo<br></br></h4>
                <p class="text-center">
                    <?= Html::a('Acceder', ['#'], ['class'=>'btn btn-primary btn-block'])?>
                </p>
            </div>
            <div class="col-md-3 text-center well well-red">
                <h4>Texto de Ejemplo<br></br></h4>

                <p>
                    <?= Html::a('Acceder', ['#'], ['class'=>'btn btn-primary btn-block'])?>
                </p>
            </div>
            <div class="col-md-3 text-center well well-red">
                <h4>Texto de Ejemplo<br></br></h4>

                <p>
                    <?= Html::a('Acceder', ['#'], ['class'=>'btn btn-primary btn-block'])?>
                </p>
            </div>
        </div>
        <div class="separador">
            <div class="col-md-6">
                <img class="posicionado-inicio" src="/LaCopa/assets/images/prueba.jpg"  alt="">
            </div>
            <div class="col-md-6">
                <p class="text-justify txt-blk centrado align-items-center">
                    For people like me, book are something solid and real, whereas digital stuff is a bit more ethereal. I like the trophy on my shelf, the presence in my home. A nice book is just as valuable. Let's face it, a hell of a lot more useful. For people like me, book are something solid and real where digital stuff is a bit more. Digital stuff is a bit more ethereal. ike the trophy on my shelf,

The presence in my home. A nice book is just as valuable. Let's face it, a hell of a lot more useful. For people like me, book are something solid and real where digital stuff.
                    For people like me, book are something solid and real, whereas digital stuff is a bit more ethereal. I like the trophy on my shelf, the presence in my home. A nice book is just as valuable. Let's face it, a hell of a lot more useful. For people like me, book are something solid and real where digital stuff is a bit more. Digital stuff is a bit more ethereal. ike the trophy on my shelf,

The presence in my home. A nice book is just as valuable. Let's face it, a hell of a lot more useful. For people like me, book are something solid and real where digital stuff.
                </p>
            </div>
        </div>
        <div class="jumbotron contenido">
            <h1>Noticias</h1>
        </div>
        <div class="col-lg-12">
            <div class="row news">
              <div class="col-md-4">
                <div class="mn-img"><img class="darkening" src="/LaCopa/assets/images/prueba.jpg" alt="website template image">
                  <div class="mn-title"><a href="https://www.free-css.com/free-css-templates">Lorem ipsum dolor sit</a></div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="mn-img"><img class="darkening" src="/LaCopa/assets/images/prueba.jpg" alt="website template image">
                  <div class="mn-title"><a href="https://www.free-css.com/free-css-templates">Lorem ipsum dolor sit</a></div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="mn-img"><img class="darkening" src="/LaCopa/assets/images/prueba.jpg" alt="website template image">
                  <div class="mn-title"><a href="https://www.free-css.com/free-css-templates">Lorem ipsum dolor sit</a></div>
                </div>
              </div>
            </div>
        </div>
        <div class="jumbotron contenido2">
            <h1>Ultimos Partidos</h1>
        </div>
    </div>
</div>
