<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>
    <?= GridView::widget([
        'dataProvider' => $resultados,
        'columns' => $campos
    ]); ?>
    <?= Html::a('Inscribir Jugador', ['jugadores/create'], ['class'=>'btn btn-primary'])?>
    <?= Html::a('Editar Lista', ['jugadores/index'], ['class'=>'btn btn-secondary'])?>