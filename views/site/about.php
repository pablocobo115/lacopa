<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--About Us-Hero Section-->
    <section class="service-area green-service">
        <div class="container">
            <div class="row">
                <h1>Nosotros</h1>
            </div>
        </div>
    </section>
    <!--/About Us-Hero Section-->

    <!--About-team Section-->
    <section class="About-team">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="team-image">
                        <img class="posicionado-arriba" src="../../../assets/images/prueba.jpg"  alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="colo-content ab-us">
                        <p>Equipo Especializado</p>
                        <h2>Pasión Por Nuestro Trabajo</h2>
                        <p>For people like me, book are something solid and
                            real, whereas digital stuff is a bit more ethereal.
                            I like the trophy on my shelf, the presence in my
                            home. A nice book is just as valuable.
                        </p>

                        <p>Let's face it, a hell of a lot more useful. For people
                            like me, book are something solid and real where
                            digital stuff is a bit more ethereal. For people.</p>

                        <div class="btn-learn bc-3">
                            <span class="learn-ab"><a href="service.html">Nuestros Servicios</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/About-team Section-->

    <!--Our-experience Section-->
    <section class="our-experience">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="o-e-content">
                        <p>Porque Elegirnos</p>
                        <h1>Experiencia En Todo Tipo De Pintura</h1>
                        <p>For people like me, book are something solid and real, whereas digital stuff
                            is a bit more ethereal. I like the trophy on my shelf, the presence in my
                            home. A nice book is just as valuable. Let's face it, a hell of a lot more useful.
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="expart">
                                <div class="expart-icon ei-green">
                                    <span class="flaticon-paint ex-1"></span>
                                </div>
                                <div class="expart-content">
                                    <h4>Color Expert</h4>
                                    <p>For people like book something
                                        real, whereas digital stuff is
                                        bit more ethereal like the troph.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="expart">
                                <div class="expart-icon ei-green">
                                    <span class="flaticon-paint-1 ex-1"></span>
                                </div>
                                <div class="expart-content">
                                    <h4>Color Expert</h4>
                                    <p>For people like book something
                                        real, whereas digital stuff is
                                        bit more ethereal like the troph.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="expart">
                                <div class="expart-icon ei-green">
                                    <span class="flaticon-graphic-tool ex-1"></span>
                                </div>
                                <div class="expart-content">
                                    <h4>Color Expert</h4>
                                    <p>For people like book something
                                        real, whereas digital stuff is
                                        bit more ethereal like the troph.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="expart">
                                <div class="expart-icon ei-green">
                                    <span class="flaticon-pantone ex-1"></span>
                                </div>
                                <div class="expart-content">
                                    <h4>Color Expert</h4>
                                    <p>For people like book something
                                        real, whereas digital stuff is
                                        bit more ethereal like the troph.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--/Our-experience Section-->

    <!-- care-about Section-->
    <section class="care-about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img class="posicionado" src="../../../assets/images/prueba.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <div class="c-a-content">
                        <p>Te Cuidamos</p>
                        <h2>Nosotros Te Ayudamos</h2>
                        <p>For people like me, book are something solid and real, whereas digital
                            stuff is a bit more ethereal. I like the trophy on my shelf, the presence
                            in my home. A nice book is just as valuable. Let's face it, a hell of a lot
                            more useful. For people like me, book are something solid and real
                            where digital stuff is a bit more. Digital stuff is a bit more ethereal.
                            ike the trophy on my shelf,</p>
                        <p>The presence in my home. A nice book is just as valuable. Let's face it,
                            a hell of a lot more useful. For people like me, book are something
                            solid and real where digital stuff.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cap-1 acp-green">
                                <div class="cap-text">
                                    <span class="cap-m"><span class="counterUp">140</span> K+</span>
                                    <p>Proyectos Finalizados</p>
                                </div>
                            </div>
                            <div class="cap-1 cap-2 acp-green">
                                <div class="cap-text">
                                    <span class="cap-m"><span class="counterUp">212</span> &nbsp;K+</span>
                                    <p>Usuarios Regulares</p>
                                </div>
                            </div>
                            <div class="cap-1 cap-3 acp-green">
                                <div class="cap-text ">
                                    <span class="cap-m"><span class="counterUp">121</span>&nbsp; +</span>
                                    <p>Premios</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /care-about Section-->
